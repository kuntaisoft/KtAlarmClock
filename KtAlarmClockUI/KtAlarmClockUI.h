/**
 * @copyright   Shanghai Kuntai Software Technology Co., Ltd. 2021
 * @license     LGPL 3.0
 * @author      Kuntai
 * @file        KtAlarmClockUI.h
 * @version		V1.0
 * @brief       Shared export macro define
 * @details     KtAlarmClockUI_EXPORTS is defined in MSVC or CMakeLists.txt file.
 * ExportedByKtAlarmClockUI is used for windows lib
 */
#ifndef _KtAlarmClockUI_EXPORT_H_
#define _KtAlarmClockUI_EXPORT_H_
#ifdef _WIN32
#if defined(KtAlarmClockUI_EXPORTS)
#define ExportedByKtAlarmClockUI __declspec(dllexport)
#else
#define ExportedByKtAlarmClockUI __declspec(dllimport)
#endif
#elif __linux__
#define ExportedByKtAlarmClockUI
#else
#error "Unknown compiler"
#endif

#endif // _KtAlarmClockUI_EXPORT_H_

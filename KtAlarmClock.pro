#-------------------------------------------------
# @copyright   Shanghai Kuntai Software Technology Co., Ltd. 2022
# @license     LGPL 3.0
# @author      Kuntai
# @file        KtAlarmClock.pro
#-------------------------------------------------
TEMPLATE = subdirs

SUBDIRS += \
    KtAlarmClockUI \
    KtAlarmClock
